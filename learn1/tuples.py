# #列表和元组的区别
#  列表的元素可任意修改，元组是不可变的，创建元组大部分用()，创建列表用[]
tuple1 = (2, 3, 5, 9, 10, 89)
print(tuple1)

# 访问方式与列表一致
print(tuple1[1], tuple1[:], tuple1[:5], tuple1[3:])

# 试图修改元组将报错
# 关于元组的类型，逗号才是起的关键作用
test1 = (1)
print(type(test1))  #int
test2 = 1, 2, 3
print(type(test2))  #tuple
# 创建空数组用()
nullTuple = ()
print(type(nullTuple))  #tuple
# 若创建一个元素的数组，则加逗号
oneElementTuple = (1,)
oneElementTuple2 = 1,
print(type(oneElementTuple), type(oneElementTuple2))

# 证明逗号起关键作用
test1 *= 1
print(test1)
oneElementTuple2 *= 3
print(oneElementTuple2)

# 更新元组
tuple1 = tuple1[:3] + ('测试添加',) + tuple1[3:]
print('更新的的元组tuple1:', tuple1)
# 删除元组内的元素
tuple1 = tuple1[:3] + tuple1[4:]
print('tuple1删除第三个位置的元素:', tuple1)

# 字符串
# 字符串操作类似元组，不可更改，也可使用分片
str1 = 'aaaabbbcdcdcd I love face'
print(str1[:], str1[:4], str1[3:])
# 更新
str1 = str1[:2] + '更新测试' + str1[2:]
print(str1)

# 字符串方法
str2 = "ceshizifuchuan897933DFAS"
# casefold()将字符串的所有字符变成小写,未改变原有字符串
print(str2.casefold(), str2)
# count()查找子字符串出现的次数,
print(str2.count('i888', 7))
# find()查找子字符串出现的次数,index()未找到会抛出异常
print(str2.find('chuan', 0, 7))
# join()连接/分隔符字符串









