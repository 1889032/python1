#-*-coding:utf-8-*-
# append()仅仅接受一个参数
empty = []
empty.append(1)
print(empty)  #1

# extend()方法向列表添加多个元素，参数为一个列表
empty.extend([3, 4, 5, 6, 7])
print(empty)  #1.3,4,5

# 在列表的任意位置插入一个数字,位置，值
empty.insert(1, 2)
print(empty)  #【1, 2， 3， 4， 5】

print(empty[0])

# 列表删除remove(), del, pop()
# remove()移除任意元素
empty.remove(5)
print(empty)

# del语句，指定删除某个位置的元素
del empty[0]
print(empty)
# del 列表名，删除整个列表元素
print("打印empty列表")
# del empty

# pop()方法删除并弹出元素,默认删除最后一个元素，可加元素索引
empty.pop()
print(empty)
empty.pop(1)
print(empty)

empty.extend([8, 10, 12, 14])
print(empty)

# 列表分片 slice,原来元素并未改变,结束元素不包含在内
print(empty[0:2])
# 如果没有开始位置，默认从0开始
print(empty[:2])
# 如果没有结束，默认到结束
print(empty[3:])
# 如果没有开始和结束，默认复制全部列表元素
list1 = empty[:]
print(list1)

# 分片步长
list2 = list1[::2]
list3 = list1[0:4:1]
print(list2, list3)
list4 = list1[::-1] #得到一个反转的列表
print(list4)

# 分片拷贝，真正的拷贝（不随着原有列表而改变）用分片拷贝而不用 = 号
test1 = [1, 3, 4, 5, 6, 9, 0]
test2 = test1[:]
print('test2值：')
print(test2)
test3 = test1
print('test3转换前的值为:', test3)
test1.reverse()
print('test1反转后test2的值为:', test2)
print('test1反转后test3的值为:', test3)

# 列表常用操作符
list5 = ['abd']
list6 = ['bcd']
print(list5 > list6)  #False, 比较第一个 字符的ASCII码

list7 = [123, 456]
list8 = [234, 123]
print(list7 > list8)
# 列表拼接
print(list7 + list8)

# 其他操作符 * *=5 in not in 只能判断一个层次的成员关系
list9 = ['ceshi1', '测试2', ['banana', 'apple'], '例子']
print('测试2' in list9)  #True
print('banana' in list9) #False
print('banana' in list9[2]) #True
print(list9[2][1])  #apple

print(dir(list))  #结果：['__add__', '__class__', '__contains__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__rmul__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']

# 列表count()方法,统计某个列表元素出现的次数，不能统计列表里的列表元素
print('出现的次数为：')
print(list9.count('apple'))

#统计出现的位置
print(list9.index('测试2'))
# 添加查找范围，不包含结束位置的元素
print(list9.index('例子', 2))

# 反转元素，原列表变化
list9.reverse()
print(list9)

# 列表元素排序 sort(),默认从小到大，改变元素
sortList = [2, 1, 4, 23, 78, 45, 99]
sortList.sort()
print(sortList)
# 从大到小排序
sortList.sort(reverse=True)
print(sortList)

print("测试print函数\n", "cehi")

"""
多行注释
多行注释
多行注释
"""








